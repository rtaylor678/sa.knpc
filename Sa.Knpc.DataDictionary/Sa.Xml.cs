﻿using System;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Xml;

using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal static void ImportExcel(string fileName, string xml)
		{
			Sa.Xsd.DataDictionary dd = new Sa.Xsd.DataDictionary();
			dd.EnforceConstraints = false;
			
			System.Console.WriteLine("Opening Excel:       " + DateTime.Now.ToString());
			Excel.Application xla = Sa.Common.NewExcelApplication();

			System.Console.WriteLine("Opening Workbook:    " + DateTime.Now.ToString());
			Excel.Workbook wkb = Sa.Common.OpenWorkbook_ReadOnly(xla, fileName);

			DateTime dt = DateTime.Now;
			System.Console.WriteLine("Importing Data:      " + dt.ToString());
			Sa.Xml.Import(dd, wkb);

			System.Console.WriteLine("Import Duration:     " + (DateTime.Now - dt).TotalSeconds.ToString());

			System.Console.WriteLine("Closing Excel:       " + DateTime.Now.ToString());
			Sa.Common.CloseExcel(ref xla, ref wkb);

			System.Console.WriteLine("Writing XML File:    " + DateTime.Now.ToString());
			Sa.Xml.WriteDocument(dd, xml);

			System.Console.WriteLine("Closing Application: " + DateTime.Now.ToString());

			dd.Clear();
			dd.Dispose();
		}

		static void Import(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
		{
			Load.Submissions(dd, wkb);

			Parallel.Invoke
			(
				() => { Load.Config(dd, wkb); },
				() => { Load.ConfigBuoy(dd, wkb); },
				() => { Load.ConfigRS(dd, wkb); },
				() => { Load.Inventory(dd, wkb); },
				() => { Load.MiscInput(dd, wkb); },
				() => { Load.ProcessData(dd, wkb); },
				() => { Load.FiredHeaters(dd, wkb); },
				() => { Load.OpexData(dd, wkb); },
				() => { Load.Pers(dd, wkb); },
				() => { Load.Absence(dd, wkb); },
				() => { Load.MaintTA(dd, wkb); },
				() => { Load.MaintRout(dd, wkb); },
				() => { Load.MExp(dd, wkb); },
				() => { Load.Crude(dd, wkb); },
				() => { Load.Yield(dd, wkb); },
				() => { Load.RPFResid(dd, wkb); },
				() => { Load.Energy(dd, wkb); },
				() => { Load.Emissions(dd, wkb); },
				() => { Load.SteamSystem(dd, wkb); },
				() => { Load.Diesel(dd, wkb); },
				() => { Load.Gasoline(dd, wkb); },
				() => { Load.MarineBunkers(dd, wkb); },
				() => { Load.Resid(dd, wkb); },
				() => { Load.Kerosene(dd, wkb); },
				() => { Load.LPG(dd, wkb); },
				() => { Load.GeneralMisc(dd, wkb); }
			);
		}

		static void WriteDocument(Sa.Xsd.DataDictionary dd, string fileName)
		{
			Encoding e = new UTF8Encoding();

			using (XmlTextWriter tw = new XmlTextWriter(fileName, e))
			{
				tw.Formatting = Formatting.Indented;

				dd.WriteXml(tw, XmlWriteMode.WriteSchema);

				tw.Flush();
				tw.Close();
			}
		}
	}
}
