﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void MiscInput(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["MiscInput"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.MiscInput.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.MiscInputRow dr = dd.MiscInput.NewMiscInputRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CDUChargeBbl = Common.ReturnDouble(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CDUChargeMT = Common.ReturnDouble(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.OffsiteEnergyPcnt = Common.ReturnSingle(rng);

					dd.MiscInput.AddMiscInputRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.MiscInput.AcceptChanges();
				dd.MiscInput.EndLoadData();
			}
		}
	}
}