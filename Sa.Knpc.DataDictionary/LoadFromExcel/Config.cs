﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Config(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Config"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Config.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.ConfigRow dr = dd.Config.NewConfigRow();
					
					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.ProcessID = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.UnitName = Common.ReturnString(rng, 50);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ProcessType = Common.ReturnString(rng, 4);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Cap = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.UtilPcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.StmCap = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.StmUtilPcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.InServicePcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.YearsOper = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MHPerWeek = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.PostPerShift = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.BlockOp = Common.ReturnString(rng, 6);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.AllocPcntofCap = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.EnergyPcnt = Common.ReturnSingle(rng);

					dd.Config.AddConfigRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Config.AcceptChanges();
				dd.Config.EndLoadData();
			}
		}
	}
}