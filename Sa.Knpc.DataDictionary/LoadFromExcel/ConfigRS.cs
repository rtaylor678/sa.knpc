﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void ConfigRS(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["ConfigRS"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.ConfigRS.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.ConfigRSRow dr = dd.ConfigRS.NewConfigRSRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.ProcessID = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ProcessType = Common.ReturnString(rng, 4);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.AvgSize = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Throughput = Common.ReturnInt16(rng);

					dd.ConfigRS.AddConfigRSRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.ConfigRS.AcceptChanges();
				dd.ConfigRS.EndLoadData();
			}
		}
	}
}