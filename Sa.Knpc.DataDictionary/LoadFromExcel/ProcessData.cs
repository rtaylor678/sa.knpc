﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void ProcessData(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["ProcessData"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.ProcessData.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.ProcessDataRow dr = dd.ProcessData.NewProcessDataRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.UnitID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Property = Common.ReturnString(rng, 8000);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RptNVal = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RptTVal = Common.ReturnString(rng, 8000);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RptDVal = Common.ReturnDateTime(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.UOM = Common.ReturnString(rng, 12);

					dd.ProcessData.AddProcessDataRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.ProcessData.AcceptChanges();
				dd.ProcessData.EndLoadData();
			}
		}
	}
}