﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Diesel(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Diesel"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Diesel.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.DieselRow dr = dd.Diesel.NewDieselRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.BlendID = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Grade = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Market = Common.ReturnString(rng, 4);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Type = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Density = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Cetane = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.PourPt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Sulfur = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.CloudPt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ASTM90 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.E350 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.BiodieselPcnt = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.KMT = Common.ReturnSingle(rng);

					dd.Diesel.AddDieselRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Diesel.AcceptChanges();
				dd.Diesel.EndLoadData();
			}
		}
	}
}