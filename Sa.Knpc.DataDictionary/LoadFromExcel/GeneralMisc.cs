﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void GeneralMisc(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["GeneralMisc"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.GeneralMisc.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.GeneralMiscRow dr = dd.GeneralMisc.NewGeneralMiscRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.FlareLossMT = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TotLossMT = Common.ReturnInt16(rng);

					dd.GeneralMisc.AddGeneralMiscRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.GeneralMisc.AcceptChanges();
				dd.GeneralMisc.EndLoadData();
			}
		}
	}
}