﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void LPG(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["LPG"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.LPG.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.LPGRow dr = dd.LPG.NewLPGRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.BlendID = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MolOrVol = Common.ReturnString(rng, 1);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolC2LT = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolC3 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolC3ene = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VoliC4 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolnC4 = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolC4ene = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.VolC5Plus = Common.ReturnSingle(rng);

					dd.LPG.AddLPGRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.LPG.AcceptChanges();
				dd.LPG.EndLoadData();
			}
		}
	}
}