﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Inventory(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Inventory"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Inventory.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.InventoryRow dr = dd.Inventory.NewInventoryRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.TankType = Common.ReturnString(rng, 3);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.TotStorage = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MktgStorage = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MandStorage = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RefStorage = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.NumTank = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.LeasedPcnt = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.AvgLevel = Common.ReturnInt16(rng);

					dd.Inventory.AddInventoryRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Inventory.AcceptChanges();
				dd.Inventory.EndLoadData();
			}
		}
	}
}