﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Yield(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Yield"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Yield.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.YieldRow dr = dd.Yield.NewYieldRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.Period = Common.ReturnString(rng, 2);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Category = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MaterialID = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MaterialName = Common.ReturnString(rng, 50);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.BBL = Common.ReturnDouble(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MT = Common.ReturnDouble(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Density = Common.ReturnSingle(rng);

					dd.Yield.AddYieldRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Yield.AcceptChanges();
				dd.Yield.EndLoadData();
			}
		}
	}
}