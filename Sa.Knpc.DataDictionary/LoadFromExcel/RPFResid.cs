﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void RPFResid(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["RPFResid"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.RPFResid.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.RPFResidRow dr = dd.RPFResid.NewRPFResidRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.EnergyType = Common.ReturnString(rng, 3);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Sulfur = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ViscTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ViscCSAtTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Density = Common.ReturnSingle(rng);

					dd.RPFResid.AddRPFResidRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.RPFResid.AcceptChanges();
				dd.RPFResid.EndLoadData();
			}
		}
	}
}