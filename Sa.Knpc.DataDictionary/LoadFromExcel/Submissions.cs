﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Submissions(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Submissions"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Submission.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.SubmissionRow dr = dd.Submission.NewSubmissionRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RefId = Common.ReturnString(rng, 8);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Date = Common.ReturnDateTime(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.UOM = Common.ReturnString(rng, 4);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RptCurrency = Common.ReturnString(rng, 4);

					dd.Submission.AddSubmissionRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Submission.AcceptChanges();
				dd.Submission.EndLoadData();
			}
		}
	}
}