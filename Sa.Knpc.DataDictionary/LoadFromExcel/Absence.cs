﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Absence(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Absence"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Absence.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.AbsenceRow dr = dd.Absence.NewAbsenceRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.CategoryID = Common.ReturnString(rng, 6);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.OCCAbs = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.MPSAbs = Common.ReturnSingle(rng);

					dd.Absence.AddAbsenceRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Absence.AcceptChanges();
				dd.Absence.EndLoadData();
			}
		}
	}
}