﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void Resid(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["Resid"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.Resid.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.ResidRow dr = dd.Resid.NewResidRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.BlendID = Common.ReturnInt16(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Grade = Common.ReturnString(rng, 5);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Density = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.Sulfur = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.PourPT = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ViscCSAtTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.ViscTemp = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.KMT = Common.ReturnSingle(rng);

					dd.Resid.AddResidRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.Resid.AcceptChanges();
				dd.Resid.EndLoadData();
			}
		}
	}
}