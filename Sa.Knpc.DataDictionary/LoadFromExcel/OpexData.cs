﻿using Excel = Microsoft.Office.Interop.Excel;

namespace Sa
{
	internal partial class Xml
	{
		internal partial class Load
		{
			internal static void OpexData(Sa.Xsd.DataDictionary dd, Excel.Workbook wkb)
			{
				int row = 17;
				int col = 2;

				Excel.Worksheet wks = wkb.Worksheets["OpexData"];
				Excel.Range rng = null;

				int r = row;
				int c = col;

				rng = wks.Cells[r, c];

				dd.OpexData.BeginLoadData();

				while (Common.RangeHasValue(rng))
				{
					Xsd.DataDictionary.OpexDataRow dr = dd.OpexData.NewOpexDataRow();

					rng = wks.Cells[r, c];
					dr.SubmissionID = Common.ReturnInt32(rng);

					rng = wks.Cells[r, ++c];
					dr.Property = Common.ReturnString(rng, 25);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.RptValue = Common.ReturnSingle(rng);

					rng = wks.Cells[r, ++c];
					if (Common.RangeHasValue(rng)) dr.OthDescription = Common.ReturnString(rng, 50);

					dd.OpexData.AddOpexDataRow(dr);

					c = col;
					rng = wks.Cells[++r, c];
				}

				dd.OpexData.AcceptChanges();
				dd.OpexData.EndLoadData();
			}
		}
	}
}