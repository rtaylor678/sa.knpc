﻿using System;

namespace Sa
{
	class GenerateXml
	{
		//static void Main(string[] args)
		static void Main()
		{
#if DEBUG
			string d = "IOFiles\\";
#else
			string d = "P:\\2013\\Refining\\KNPC\\PMSA (3RM603N)\\Project Model\\KNPC_DOCUMENTATION\\DataDictionary Deliverable\\";
#endif

			string i = d + "TORPMSA-D-PM-03.01.01 Data Dictionary 20122013 v1.0.xlsm";
			string o = d + "TORPMSA-D-PM-03.01.01 Data Dictionary 20122013 v1.1.xml";

			Sa.Xml.ImportExcel(i, o);

			Console.WriteLine("Press any key to continue...");
			Console.ReadKey();
		}
	}
}
